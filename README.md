### Node dev tests
#### Running SortAndSearch examples
Install ts-node
```bash
npm install -g ts-node
npm install -g typescript
```

Run examples:
```bash
ts-node SortAndSearch/index.ts
```
