// Merge Sort
export class ASort {
  static sort(arr: number[]) {
    if (arr.length <= 1) {
      return [...arr];
    }

    const middle = arr.length / 2;

    const left = arr.slice(0, middle);
    const right = arr.slice(middle, arr.length);

    return this.merge(this.sort(left), this.sort(right));
  }

  private static merge(left: number[], right: number[]) {
    const sorted = [];
    while(left.length > 0 && right.length > 0) {
      if (left[0] <= right[0]) {
        sorted.push(left[0]);
        left = left.slice(1, left.length);
      } else {
        sorted.push(right[0]);
        right = right.slice(1, right.length);
      }
    }

    if (left.length > 0) {
      return [...sorted, ...left];
    }

    if (right.length > 0) {
      return [...sorted, ...right];
    }

    return sorted;
  }
}
