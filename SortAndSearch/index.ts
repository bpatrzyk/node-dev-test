import { ASort } from './ASort';
import { BSort } from './BSort';
import { BSearch } from './BSearch';
import { areEqual } from './helpers'

const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
const elementsToFind = [1, 5, 13, 27, 77];

const originalUnsorted = [...unsorted];

const sortedJS = [...unsorted].sort((a, b) => a - b);

const sortedA = ASort.sort(unsorted);
console.assert(areEqual(sortedA, sortedJS), 'Asort failed to sort array');
console.assert(areEqual(unsorted, originalUnsorted), 'Asort modified original array');

const sortedB = BSort.sort(unsorted);
console.assert(areEqual(sortedB, sortedJS), 'BSort failed to sort array');
console.assert(areEqual(unsorted, originalUnsorted), 'BSort modified original array');

const expectedSearchResults = [-1, 3, 6, -1, 12];
const expectedOperationsCount = [4, 8, 9, 13, 16];

for (let i = 0; i < elementsToFind.length; i++) {
  let result = BSearch.getInstance().search(sortedA, elementsToFind[i]);
  console.assert(result === expectedSearchResults[i], `BSearch failed for value ${elementsToFind[i]}`);
  console.assert(BSearch.getInstance().operations === expectedOperationsCount[i], `BSearch operations count mismatch for value ${elementsToFind[i]}`);
}
