// Selection Sort
export class BSort {
  static sort(arr: number[]) {
    const sorted = [...arr];

    for (let i = 0; i < sorted.length - 1; i++) {
      let { minIdx, minVal } = this.findMin(sorted, i + 1);
      if (minVal < sorted[i]) {
        this.swap(sorted, i, minIdx);
      }
    }

    return sorted;
  }

  private static findMin(arr: number[], from: number) {
    let minVal = null;
    let minIdx = null;

    for (let i = from; i < arr.length; i++) {
      if (minVal === null || minVal > arr[i]) {
        minVal = arr[i];
        minIdx = i;
      }
    }
    return { minVal, minIdx };
  }

  private static swap(arr: number[], i: number, j: number) {
    const tmp = arr[i];
    arr[i] = arr[j];
    arr[j] = tmp;
  }
}
