export class BSearch {
  private static instance: BSearch;
  // It holds the number of operations performed since the creation of the singleton instance, not since the last use of search function
  private _operations = 0;

  private constructor() {
  }

  static getInstance(): BSearch {
    if (!BSearch.instance) {
      BSearch.instance = new BSearch();
    }
    return BSearch.instance;
  }

  search(arr: number[], value: number) {
    let left = 0;
    let right = arr.length - 1;

    while (left <= right) {
      this._operations++;

      const middle = Math.floor((left + right) / 2);
      if (arr[middle] < value) {
        left = middle + 1;
      } else if (arr[middle] > value) {
        right = middle - 1;
      } else {
        return middle;
      }
    }

    return -1;
  }

  get operations(): number {
    return this._operations;
  }
}
