import request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { ReportModule } from '../src/Report/ReportModule';

describe('Reports', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [ReportModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(404);
  });

  describe('products', () => {
    it('should return the best selling product for the specified date', () => {
      return request(app.getHttpServer())
        .get('/report/products/2019-08-07')
        .expect(200)
        .expect({
          productName: 'Black sport shoes',
          quantity: 2,
          totalPrice: 220,
        });
    });

    it('should return 404 if no orders are found for the specified date', () => {
      return request(app.getHttpServer())
        .get('/report/products/2019-08-06')
        .expect(404)
        .expect({
          statusCode: 404,
          message: 'No orders found for date 2019-08-06',
        });
    });
  });

  describe('customer', () => {
    it('should return the best customers for the specified date', () => {
      return request(app.getHttpServer())
        .get('/report/customer/2019-08-08')
        .expect(200)
        .expect({
          customerName: 'Jane Doe',
          totalPrice: 110,
        });
    });

    it('should return 404 if no orders are found for the specified date', () => {
      return request(app.getHttpServer())
        .get('/report/customer/2019-08-06')
        .expect(404)
        .expect({
          statusCode: 404,
          message: 'No orders found for date 2019-08-06',
        });
    });
  });
});
