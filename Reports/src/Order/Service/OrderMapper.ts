import { Injectable, Inject } from '@nestjs/common';
import _ from 'lodash';
import { Repository } from './Repository';
import { Order } from '../Model/Order';
import { Product } from '../Model/Product';
import { Customer } from '../Model/Customer';

// I would prefer to create a separate OrderService that would retrieve data from Repository and map it to models
// using OrderMapper
// What is more, I would reimplement the repository to use proper database functionalities to optimize the requests

@Injectable()
export class OrderMapper {
  @Inject() repository: Repository;

  async getOrders(): Promise<Order[]> {
    const orders = await this.repository.fetchOrders();
    return orders as Order[];
  }

  async getOrdersByDate(date: string): Promise<Order[]> {
    const orders = await this.getOrders();
    return _.filter(orders, order => order.createdAt === date);
  }

  async getProducts(): Promise<Product[]> {
    const products = await this.repository.fetchProducts();
    return products as Product[];
  }

  async getProduct(id: number) {
    const products = await this.getProducts();
    return _.find(products, product => product.id === id);
  }

  async getCustomers(): Promise<Customer[]> {
    const customers = await this.repository.fetchCustomers();
    return customers as Customer[];
  }
}
