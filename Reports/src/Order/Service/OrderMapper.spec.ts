import { Test } from '@nestjs/testing';
import { OrderMapper } from './OrderMapper';
import { Repository } from './Repository';

const orders = [
  {
    number: '2019/07/1',
    customer: 1,
    createdAt: '2019-08-07',
    products: [1, 2],
  },
  {
    number: '2019/07/2',
    customer: 2,
    createdAt: '2019-08-07',
    products: [1],
  },
  {
    number: '2019/08/1',
    customer: 2,
    createdAt: '2019-08-08',
    products: [1],
  },
  {
    number: '2019/08/2',
    customer: 1,
    createdAt: '2019-08-08',
    products: [2],
  },
  {
    number: '2019/08/3',
    customer: 1,
    createdAt: '2019-08-08',
    products: [3],
  },
];

const products = [
  { id: 1, name: 'Black sport shoes', price: 110 },
  { id: 2, name: 'Cotton t-shirt XL', price: 25.75 },
  { id: 3, name: 'Blue jeans', price: 55.99 },
];

describe('OrderMapper', () => {
  let orderMapper: OrderMapper;
  let repository: Repository;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [Repository, OrderMapper],
    }).compile();

    orderMapper = module.get<OrderMapper>(OrderMapper);
    repository = module.get<Repository>(Repository);
  });

  describe('getOrders', () => {
    it('should return an array of orders', async () => {
      jest.spyOn(repository, 'fetchOrders').mockImplementation(() => new Promise(resolve => resolve(orders)));

      expect(await orderMapper.getOrders()).toEqual(orders);
    });
  });

  describe('getOrdersByDate', () => {
    it('should return orders for a specified date', async () => {
      jest.spyOn(repository, 'fetchOrders').mockImplementation(() => new Promise(resolve => resolve(orders)));

      expect(await orderMapper.getOrdersByDate('2019-08-07')).toEqual([
        {
          number: '2019/07/1',
          customer: 1,
          createdAt: '2019-08-07',
          products: [1, 2],
        },
        {
          number: '2019/07/2',
          customer: 2,
          createdAt: '2019-08-07',
          products: [1],
        },
      ]);
    });

    it('should return empty list if there are no orders for a specified date', async () => {
      jest.spyOn(repository, 'fetchOrders').mockImplementation(() => new Promise(resolve => resolve(orders)));

      expect(await orderMapper.getOrdersByDate('2019-08-06')).toEqual([]);
    });
  });

  describe('getProducts', () => {
    it('should return an array of products', async () => {
      jest.spyOn(repository, 'fetchProducts').mockImplementation(() => new Promise(resolve => resolve(products)));

      expect(await orderMapper.getProducts()).toEqual(products);
    });
  });

  describe('getProduct', () => {
    it('should return a product by id', async () => {
      jest.spyOn(repository, 'fetchProducts').mockImplementation(() => new Promise(resolve => resolve(products)));

      expect(await orderMapper.getProduct(1)).toEqual(products[0]);
    });

    it('should return undefined if product is not found', async () => {
      jest.spyOn(repository, 'fetchProducts').mockImplementation(() => new Promise(resolve => resolve(products)));

      expect(await orderMapper.getProduct(66)).toEqual(undefined);
    });
  });

  describe('getCustomers', () => {
    it('should return an array of customers', async () => {
      const customer = {
        id: 1,
        firstName: 'John',
        lastName: 'Doe',
      };

      jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => new Promise(resolve => resolve([customer])));

      expect(await orderMapper.getCustomers()).toEqual([customer]);
    });
  });
});
