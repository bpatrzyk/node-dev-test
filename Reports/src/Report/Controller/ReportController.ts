import { Controller, Get, HttpException, Inject, Param } from '@nestjs/common';
import _ from 'lodash';
import { IBestBuyers, IBestSellers } from '../Model/IReports';
import { OrderMapper } from '../../Order/Service/OrderMapper';

@Controller()
export class ReportController {
  @Inject() orderMapper: OrderMapper;

  @Get('/report/products/:date')
  async bestSellers(@Param('date') date: string): Promise<IBestSellers> {
    const orders = await this.orderMapper.getOrdersByDate(date);
    if (orders.length === 0) {
      throw new HttpException(`No orders found for date ${date}`, 404);
    }

    const allProducts = _.flatMap(orders, order => order.products);
    const productsCount = _.countBy(allProducts);
    const sortedProductCount = _.map(productsCount, (count, id) => ({
      id: parseInt(id, 10),
      count,
    })).sort((a, b) => b.count - a.count);

    const highestProductCount = sortedProductCount[0].count;
    const bestSellingProducts = await Promise.all(_.filter(sortedProductCount, product => product.count === highestProductCount)
      .map(async (bestSeller) => {
        const product = await this.orderMapper.getProduct(bestSeller.id);
        return {
          ...bestSeller,
          totalPrice: bestSeller.count * product.price,
          productName: product.name,
        };
      }));

    bestSellingProducts.sort((a, b) => b.totalPrice - a.totalPrice);

    return {
      productName: bestSellingProducts[0].productName,
      quantity: bestSellingProducts[0].count,
      totalPrice: bestSellingProducts[0].totalPrice,
    };
  }

  @Get('/report/customer/:date')
  async bestBuyers(@Param('date') date: string): Promise<IBestBuyers> {
    const orders = await this.orderMapper.getOrders();
    const customers = await this.orderMapper.getCustomers();
    const products = await this.orderMapper.getProducts();

    const ordersOnDate = orders.filter((order) => order.createdAt === date);
    if (ordersOnDate.length === 0) {
      throw new HttpException(`No orders found for date ${date}`, 404);
    }

    const ordersWithTotal = ordersOnDate.map(order => {
      const totalPrice = order.products.reduce((sum, productId) => {
        const price = _.find(products, prod => prod.id === productId).price;
        return sum + price;
      }, 0);
      return {
        ...order,
        totalPrice,
      };
    }).sort((a, b) => b.totalPrice - a.totalPrice);

    const customer = _.find(customers, c => c.id === ordersWithTotal[0].customer);

    return {
      customerName: `${customer.firstName} ${customer.lastName}`,
      totalPrice: ordersWithTotal[0].totalPrice,
    };
  }
}
