import { Test } from '@nestjs/testing';
import { ReportController } from './ReportController';
import { OrderMapper } from '../../Order/Service/OrderMapper';
import { OrderModule } from '../../Order/OrderModule';
import { Order } from '../../Order/Model/Order';
import { Product } from '../../Order/Model/Product';
import { Customer } from '../../Order/Model/Customer';

const orders = [
  {
    number: '2019/07/1',
    customer: 1,
    createdAt: '2019-08-07',
    products: [1, 2],
  },
  {
    number: '2019/07/2',
    customer: 2,
    createdAt: '2019-08-07',
    products: [1],
  },
  {
    number: '2019/08/1',
    customer: 2,
    createdAt: '2019-08-08',
    products: [1],
  },
  {
    number: '2019/08/2',
    customer: 1,
    createdAt: '2019-08-08',
    products: [2],
  },
  {
    number: '2019/08/3',
    customer: 1,
    createdAt: '2019-08-08',
    products: [3],
  },
] as Order[];

const products = [
  { id: 1, name: 'Black sport shoes', price: 110 },
  { id: 2, name: 'Cotton t-shirt XL', price: 25.75 },
  { id: 3, name: 'Blue jeans', price: 55.99 },
] as Product[];

const customers = [
  {
    id: 1,
    firstName: 'John',
    lastName: 'Doe',
  },
  {
    id: 2,
    firstName: 'Jane',
    lastName: 'Doe',
  },
] as Customer[];

describe('OrderMapper', () => {
  let reportController: ReportController;
  let orderMapper: OrderMapper;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [OrderModule],
      controllers: [ReportController],
      providers: [],
    }).compile();

    reportController = module.get<ReportController>(ReportController);
    orderMapper = module.get<OrderMapper>(OrderMapper);
  });

  describe('bestSellers', () => {
    it('should return the best selling products based on quantity', async () => {
      jest.spyOn(orderMapper, 'getOrders').mockImplementation(() => new Promise(resolve => resolve(orders)));
      jest.spyOn(orderMapper, 'getProducts').mockImplementation(() => new Promise(resolve => resolve(products)));

      const result = await reportController.bestSellers('2019-08-07');

      expect(result).toEqual({
        productName: 'Black sport shoes',
        quantity: 2,
        totalPrice: 220,
      });
    });

    it('should return the best selling products based on total value if multiple product have the same quantity', async () => {
      jest.spyOn(orderMapper, 'getOrders').mockImplementation(() => new Promise(resolve => resolve(orders)));
      jest.spyOn(orderMapper, 'getProducts').mockImplementation(() => new Promise(resolve => resolve(products)));

      const result = await reportController.bestSellers('2019-08-08');

      expect(result).toEqual({
        productName: 'Black sport shoes',
        quantity: 1,
        totalPrice: 110,
      });
    });

    it('should throw exception if no orders are found for the specified date', async () => {
      jest.spyOn(orderMapper, 'getOrders').mockImplementation(() => new Promise(resolve => resolve(orders)));
      jest.spyOn(orderMapper, 'getProducts').mockImplementation(() => new Promise(resolve => resolve(products)));

      expect.assertions(2);
      try {
        await reportController.bestSellers('2019-08-06');
      } catch (e) {
        expect(e.message).toEqual('No orders found for date 2019-08-06');
        expect(e.getStatus()).toEqual(404);
      }
    });
  });

  describe('bestBuyers', () => {
    it('should return the best buyer', async () => {
      jest.spyOn(orderMapper, 'getOrders').mockImplementation(() => new Promise(resolve => resolve(orders)));
      jest.spyOn(orderMapper, 'getCustomers').mockImplementation(() => new Promise(resolve => resolve(customers)));

      const result = await reportController.bestBuyers('2019-08-08');

      expect(result).toEqual({
        customerName: 'Jane Doe',
        totalPrice: 110,
      });
    });

    it('should throw exception if no orders are found for the specified date', async () => {
      jest.spyOn(orderMapper, 'getOrders').mockImplementation(() => new Promise(resolve => resolve(orders)));
      jest.spyOn(orderMapper, 'getProducts').mockImplementation(() => new Promise(resolve => resolve(products)));

      expect.assertions(2);
      try {
        await reportController.bestBuyers('2019-08-06');
      } catch (e) {
        expect(e.message).toEqual('No orders found for date 2019-08-06');
        expect(e.getStatus()).toEqual(404);
      }
    });
  });
});
